var arr = [];
for (var i = 0; i < 10; i++) {
  arr.push(Number(prompt("Enter a 10 number to create an Array :")));
}

console.log(arr);

function createArray(arr) {
  for (let i = 1; i < arr.length; i += 2) {
    temp = arr[i - 1];
    arr[i - 1] = arr[i];
    arr[i] = temp;
  }
  return arr;
}

const newArray = createArray(arr);
const resArray = [];
resArray.push(newArray);

document.querySelector(
  "#newArray"
).innerHTML = `Create A New Array  : [${resArray}]`;

console.log(resArray);
