///////////////////////////
// How many Fridays till the end of the Year

function getWeeksDiff() {
  const startDate = new Date();
  console.log(startDate);
  const currentYear = new Date().getFullYear();
  const endDate = new Date(currentYear, 11, 31).getTime();
  let days = Math.floor((endDate - startDate) / (1000 * 60 * 60 * 24 * 7));
  return days;
}

console.log(getWeeksDiff());

const fridaysTillEndYear = `${getWeeksDiff()} Fridays till the End of The Year!! 🎅 `;
document.querySelector("#countDownFridays").innerHTML = fridaysTillEndYear;
